/*
This software is subject to the license described in the license.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
*/

/**@file
 * @defgroup nrf_ant_background_scanning_demo ANT Background Scanning Example
 * @{
 * @ingroup nrf_ant_background_scanning_demo
 *
 * @brief Example of ANT Background Scanning implementation.
 *
 * Before compiling this example for NRF52, complete the following steps:
 * - Download the S212 SoftDevice from <a href="https://www.thisisant.com/developer/components/nrf52832" target="_blank">thisisant.com</a>.
 * - Extract the downloaded zip file and copy the S212 SoftDevice headers to <tt>\<InstallFolder\>/components/softdevice/s212/headers</tt>.
 * If you are using Keil packs, copy the files into a @c headers folder in your example folder.
 * - Make sure that @ref ANT_LICENSE_KEY in @c nrf_sdm.h is uncommented.
 */

#include <stdio.h>
#include <stdlib.h>
#include "nrf.h"
#include "bsp.h"
#include "app_uart.h"
#include "ant_interface.h"
#include "ant_parameters.h"
#include "nrf_soc.h"
#include "nrf_sdm.h"
#include "app_error.h"
#include "app_util.h"
#include "nordic_common.h"
#include "ant_stack_config.h"
#include "ant_channel_config.h"
#include "ant_search_config.h"
#include "app_trace.h"
#include "app_timer.h"
#include "nrf_delay.h"
#include "softdevice_handler.h"


#define APP_TIMER_PRESCALER         0x00                    /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE     0x04                    /**< Size of timer operation queues. */

#define ANT_MS_CHANNEL_NUMBER       ((uint8_t) 1)           /**< Master channel. */
#define ANT_BS_CHANNEL_NUMBER       ((uint8_t) 0)           /**< Background scanning channel. */

#define ANT_NETWORK_NUMBER          ((uint8_t) 0)           /**< Default public network number. */

#define ANT_MS_CHANNEL_TYPE         CHANNEL_TYPE_MASTER     /**< Bi-directional master. */
#define ANT_BS_CHANNEL_TYPE         CHANNEL_TYPE_SLAVE      /**< Bi-directional slave. */

#define ANT_BS_DEVICE_NUMBER        ((uint16_t) 0)          /**< Wild-card. */

#define ANT_DEVICE_TYPE             ((uint8_t) 1)           /**< Device type. */
#define ANT_TRANSMISSION_TYPE       ((uint8_t) 5)           /**< Transmission type. */
#define ANT_FREQUENCY               ((uint8_t) 77)          /**< 2477 MHz. */

#define ANT_CHANNEL_PERIOD          ((uint16_t) 2048)       /**< 16 Hz. */
#define ANT_CHANNEL_PERIOD_NONE     ((uint16_t) 0x00)       /**< This is not taken into account. */

#define ANT_BEACON_PAGE             ((uint8_t) 1)

#define ANT_DATA_PAYLOAD_SIZE_24	((uint8_t) 24)

//--- UART MARCOS ---/
#define UART_TX_BUF_SIZE                512                 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                512                 /**< UART RX buffer size. */




void ant_message_send(uint8_t *tx_bufferz);
void background_scanner_process(ant_evt_t * p_ant_evt);
void master_beacon_process(ant_evt_t * p_ant_evt);

static uint8_t m_last_rssi       = 0;
static uint16_t m_last_device_id = 0;
uint8_t tx_buffer[ANT_STANDARD_DATA_PAYLOAD_SIZE];

/**< Derive from device serial number. */
static uint16_t ant_ms_dev_num_get(void)
{
   return ((uint16_t) (NRF_FICR->DEVICEID[0]));
}


#define NUM_OF_CHAN	9
//double counterAvg[NUM_OF_CHAN] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
double counterAvg[NUM_OF_CHAN][NUM_OF_CHAN];
/*------------------- ROBOT NUM -> IDs -------------------/
robot 0 -> ID: 235
robot 1 -> ID: 127
robot 2 -> ID: 202
robot 3 -> ID: 4
robot 4 -> ID: 117
robot 5 -> ID: 211
robot 6 -> ID: 122
robot 7 -> ID: 230
robot 8 -> ID: 223
// below is the array of robot # (index) -> ID*/
uint16_t robotNum[9] = {235, 127, 202, 4, 117, 211, 122, 230, 223};
//-------------------------- END --------------------------/


//--- master matrix of estimated data for all robots ---/
uint8_t estimatedProxData[NUM_OF_CHAN][NUM_OF_CHAN];
uint8_t estimatedProxDataAvg[NUM_OF_CHAN][NUM_OF_CHAN];
uint16_t estimatedBearData[NUM_OF_CHAN][NUM_OF_CHAN];
uint16_t estimatedBearDataAvg[NUM_OF_CHAN][NUM_OF_CHAN];
//------------------------ END ------------------------/


/**@brief Function for dispatching an ANT stack event to all modules with an ANT stack event handler.
 *
 * @details This function is called from the ANT stack event interrupt handler after an ANT stack
 *          event has been received.
 *
 * @param[in] p_ant_evt  ANT stack event.
 */
void ant_evt_dispatch(ant_evt_t * p_ant_evt)
{
   switch (p_ant_evt->channel)
   {
      case ANT_BS_CHANNEL_NUMBER:
         background_scanner_process(p_ant_evt);
         break;

      case ANT_MS_CHANNEL_NUMBER:
         master_beacon_process(p_ant_evt);
         break;

      default:
         break;
   }
}


/**@brief Initialize application.
 */
static void application_initialize()
{
   /* Set library config to report RSSI and Device ID */
   uint32_t err_code = sd_ant_lib_config_set(ANT_LIB_CONFIG_MESG_OUT_INC_RSSI
                                         | ANT_LIB_CONFIG_MESG_OUT_INC_DEVICE_ID);
   APP_ERROR_CHECK(err_code);

   const uint16_t dev_num = ant_ms_dev_num_get();

   const ant_channel_config_t ms_channel_config =
   {
      .channel_number    = ANT_MS_CHANNEL_NUMBER,
      .channel_type      = ANT_MS_CHANNEL_TYPE,
      .ext_assign        = 0x00,
      .rf_freq           = ANT_FREQUENCY,
      .transmission_type = ANT_TRANSMISSION_TYPE,
      .device_type       = ANT_DEVICE_TYPE,
      .device_number     = dev_num,
      .channel_period    = ANT_CHANNEL_PERIOD,//ANT_CHANNEL_PERIOD_NONE,//ANT_CHANNEL_PERIOD,
      .network_number    = ANT_NETWORK_NUMBER,
   };

   const ant_channel_config_t bs_channel_config =
   {
      .channel_number    = ANT_BS_CHANNEL_NUMBER,
      .channel_type      = ANT_BS_CHANNEL_TYPE,
      .ext_assign        = EXT_PARAM_ALWAYS_SEARCH,
      .rf_freq           = ANT_FREQUENCY,
      .transmission_type = ANT_TRANSMISSION_TYPE,
      .device_type       = ANT_DEVICE_TYPE,
      .device_number     = ANT_BS_DEVICE_NUMBER,
      .channel_period    = ANT_CHANNEL_PERIOD_NONE,
      .network_number    = ANT_NETWORK_NUMBER,
   };

   const ant_search_config_t bs_search_config =
   {
      .channel_number        = ANT_BS_CHANNEL_NUMBER,
      .low_priority_timeout  = ANT_LOW_PRIORITY_TIMEOUT_DISABLE,
      .high_priority_timeout = ANT_HIGH_PRIORITY_SEARCH_DISABLE,
      .search_sharing_cycles = ANT_SEARCH_SHARING_CYCLES_DISABLE,
      .search_priority       = ANT_SEARCH_PRIORITY_DEFAULT,
      .waveform              = ANT_WAVEFORM_DEFAULT,
   };

   err_code = ant_channel_init(&ms_channel_config);
   APP_ERROR_CHECK(err_code);

   err_code = ant_channel_init(&bs_channel_config);
   APP_ERROR_CHECK(err_code);

   err_code = ant_search_init(&bs_search_config);
   APP_ERROR_CHECK(err_code);

   // Fill tx buffer for the first frame
   //ant_message_send();

   err_code = sd_ant_channel_open(ANT_MS_CHANNEL_NUMBER);
   APP_ERROR_CHECK(err_code);

   err_code = sd_ant_channel_open(ANT_BS_CHANNEL_NUMBER);
   APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Tracer initialization.
 */
static void utils_setup(void)
{
   uint32_t err_code;

   app_trace_init();

   APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
   err_code = bsp_init(BSP_INIT_LED,
   APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
   NULL);
   APP_ERROR_CHECK(err_code);
}


/**@brief Function for ANT stack initialization.
 *
 * @details Initializes the SoftDevice and the ANT event interrupt.
 */
static void softdevice_setup(void)
{
   uint32_t err_code;

   nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

   err_code = softdevice_ant_evt_handler_set(ant_evt_dispatch);
   APP_ERROR_CHECK(err_code);

   err_code = softdevice_handler_init(&clock_lf_cfg, NULL, 0, NULL);
   APP_ERROR_CHECK(err_code);

   err_code = ant_stack_static_config();
   APP_ERROR_CHECK(err_code);
}


/*
static float avgNum1 = 0.0;
static float avgNum2 = 0.0;
static double counter = 0.0;*/


/**@brief Process ANT message on ANT background scanning channel.
 *
 * @param[in] p_ant_event ANT message content.
 */
uint8_t prevRX_MSG[NUM_OF_CHAN][8];

void background_scanner_process(ant_evt_t * p_ant_evt)
{
   uint32_t      err_code;
   ANT_MESSAGE * p_ant_message = (ANT_MESSAGE*)p_ant_evt->msg.evt_buffer;

   switch(p_ant_evt->event)
   {
      case EVENT_RX:
      {
         err_code = bsp_indication_set(BSP_INDICATE_RCV_OK);
         APP_ERROR_CHECK(err_code);

         if(p_ant_message->ANT_MESSAGE_stExtMesgBF.bANTDeviceID)
         {
             m_last_device_id = uint16_decode(p_ant_message->ANT_MESSAGE_aucExtData);
         }

         if(p_ant_message->ANT_MESSAGE_stExtMesgBF.bANTRssi)
         {
             m_last_rssi = p_ant_message->ANT_MESSAGE_aucExtData[5];
         }

         //--- check which robot send the message ---/
         uint8_t curRobotNum = 0;
         for (; curRobotNum < NUM_OF_CHAN; curRobotNum++) {
				// store the correct robot's estimated data
				if (robotNum[curRobotNum] == m_last_device_id) {
               // leave the for loop since we found our robot
               break;
            }
         }


         //--- check if the message is repeated message; ignore if it is ---/
         uint8_t newMsg = 0;
         for (uint8_t i = 0; i < 8; i++) {
            if (prevRX_MSG[curRobotNum][i] != p_ant_message->ANT_MESSAGE_aucPayload[i]) {
               // it is not the same message! continue on and store this new msg
               for (uint8_t j = 0; j < 8; j++) {
                  prevRX_MSG[curRobotNum][j] = p_ant_message->ANT_MESSAGE_aucPayload[i];
                  newMsg = 1;
               }
               break;
            }
         }
         // no new message, IGNORE!
         if (!newMsg) {
            break;
         }


			//--- decode the message into readable RX message ---/
#define CLOSE_CHECK	3
			uint8_t robNums[CLOSE_CHECK];
			uint8_t proxData[CLOSE_CHECK];
			uint16_t bearData[CLOSE_CHECK];

			proxData[0] = p_ant_message->ANT_MESSAGE_aucPayload[0];
			proxData[1] = p_ant_message->ANT_MESSAGE_aucPayload[1];
			proxData[2] = p_ant_message->ANT_MESSAGE_aucPayload[2];
			robNums[0] = ((p_ant_message->ANT_MESSAGE_aucPayload[3]>>4) & 0b00001111) & 0xFF;
			robNums[1] = (p_ant_message->ANT_MESSAGE_aucPayload[3] & 0b00001111) & 0xFF;
			robNums[2] = (p_ant_message->ANT_MESSAGE_aucPayload[4] & 0b00001111) & 0xFF;
			bearData[0] = (((p_ant_message->ANT_MESSAGE_aucPayload[4]>>4) & 0b000001111) | (((p_ant_message->ANT_MESSAGE_aucPayload[5])<<1) & 0b111110000)) & 0b111111111;
			bearData[1] = ((p_ant_message->ANT_MESSAGE_aucPayload[5] & 0b000000111) | ((p_ant_message->ANT_MESSAGE_aucPayload[6]<<3) & 0b111111000)) & 0b111111111;
			bearData[2] = ((p_ant_message->ANT_MESSAGE_aucPayload[7]<<1) | ((p_ant_message->ANT_MESSAGE_aucPayload[6]>>7) & 0b1)) & 0b111111111;


			//--- store message information into master matrix ---/
			// reset the master data matrix for proximity first
			for (uint8_t j = 0; j < NUM_OF_CHAN; j++) {
				estimatedProxData[curRobotNum][j] = 0xFF;	// set it to the highest possible value
			}
			// store the proximity data
			estimatedProxData[curRobotNum][robNums[0]] = proxData[0];
			estimatedProxData[curRobotNum][robNums[1]] = proxData[1];
			estimatedProxData[curRobotNum][robNums[2]] = proxData[2];
			// store the bearing data
			estimatedBearData[curRobotNum][robNums[0]] = bearData[0];
			estimatedBearData[curRobotNum][robNums[1]] = bearData[1];
			estimatedBearData[curRobotNum][robNums[2]] = bearData[2];


         //--- average out the current data readings with past history ---/
#define CUTOFF_DISTANCE    255   // in mm
         for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
            // only count it towards the average if it was one of the robNums[]
            if ((robNums[0] == i) || (robNums[1] == i) || (robNums[2] == i)) {
               if (counterAvg[curRobotNum][i] < 1000.0) {
                  // only count towards the estimated avg if below CUTOFF_DISTANCE
                  if (estimatedProxData[curRobotNum][i] <= CUTOFF_DISTANCE) {
                     estimatedProxDataAvg[curRobotNum][i] = (((double)estimatedProxData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedProxDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                     // deal with bearing averaging corner case
                     if ((estimatedBearDataAvg[curRobotNum][i] >= 270) && ((estimatedBearData[curRobotNum][i] >= 0) && (estimatedBearData[curRobotNum][i] <= 180))) {
                        estimatedBearData[curRobotNum][i] += 360;
                        estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                        // ensure average ranges from 0 to 360
                        if (estimatedBearDataAvg[curRobotNum][i] >= 360) {
                           estimatedBearDataAvg[curRobotNum][i] -= 360;
                        }
                     }
                     else if ((estimatedBearDataAvg[curRobotNum][i] <= 90) && ((estimatedBearData[curRobotNum][i] >= 180) && (estimatedBearData[curRobotNum][i] <= 360))) {
                        estimatedBearDataAvg[curRobotNum][i] += 360;
                        estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                        // ensure average ranges from 0 to 360
                        if (estimatedBearDataAvg[curRobotNum][i] >= 360) {
                           estimatedBearDataAvg[curRobotNum][i] -= 360;
                        }
                     }
                     else {
                        estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                     }
                     counterAvg[curRobotNum][i]++;
                  }
                  else {
                     // do nothing...
                  }
               }
               // counter has reached max limit, so don't increment and only calculate
               else {
                  estimatedProxDataAvg[curRobotNum][i] = (((double)estimatedProxData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedProxDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                  // deal with bearing averaging corner case
                  if ((estimatedBearDataAvg[curRobotNum][i] >= 270) && ((estimatedBearData[curRobotNum][i] >= 0) && (estimatedBearData[curRobotNum][i] <= 180))) {
                     estimatedBearData[curRobotNum][i] += 360;
                     estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                     // ensure average ranges from 0 to 360
                     if (estimatedBearDataAvg[curRobotNum][i] >= 360) {
                        estimatedBearDataAvg[curRobotNum][i] -= 360;
                     }
                  }
                  else if ((estimatedBearDataAvg[curRobotNum][i] <= 90) && ((estimatedBearData[curRobotNum][i] >= 180) && (estimatedBearData[curRobotNum][i] <= 360))) {
                     estimatedBearDataAvg[curRobotNum][i] += 360;
                     estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                     // ensure average ranges from 0 to 360
                     if (estimatedBearDataAvg[curRobotNum][i] >= 360) {
                        estimatedBearDataAvg[curRobotNum][i] -= 360;
                     }
                  }
                  else {
                     estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum][i])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum][i] - 1.0)/counterAvg[curRobotNum][i]));
                  }
               }
            }
         }

         /*if (counterAvg[curRobotNum] < 1000.0) {
            for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
               estimatedProxDataAvg[curRobotNum][i] = (((double)estimatedProxData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum])) + (estimatedProxDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum] - 1.0)/counterAvg[curRobotNum]));
               estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum] - 1.0)/counterAvg[curRobotNum]));
            }
            counterAvg[curRobotNum]++;
         }
         // reached maximum counter average...
         else {
            for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
               estimatedProxDataAvg[curRobotNum][i] = (((double)estimatedProxData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum])) + (estimatedProxDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum] - 1.0)/counterAvg[curRobotNum]));
               estimatedBearDataAvg[curRobotNum][i] = (((double)estimatedBearData[curRobotNum][i]) * (1.0 / counterAvg[curRobotNum])) + (estimatedBearDataAvg[curRobotNum][i] * ((counterAvg[curRobotNum] - 1.0)/counterAvg[curRobotNum]));
            }
         }*/


         break;
      }

      default:
      {
         break;
      }
   }
}


/**@brief Function for setting payload for ANT message and sending it via
 *        ANT master beacon channel.
 *
 *
 * @details   ANT_BEACON_PAGE message is queued. The format is:
 *            byte[0]   = page (1 = ANT_BEACON_PAGE)
 *            byte[1]   = last RSSI value received
 *            byte[2-3] = channel ID of device corresponding to last RSSI value (little endian)
 *            byte[6]   = counter that increases with every message period
 *            byte[7]   = number of messages received on background scanning channel
 */
void ant_message_send(uint8_t *tx_bufferz)
{
  uint32_t       err_code;

  err_code = sd_ant_broadcast_message_tx(ANT_MS_CHANNEL_NUMBER,
                                         ANT_STANDARD_DATA_PAYLOAD_SIZE,
                                         tx_bufferz);
  APP_ERROR_CHECK(err_code);
}



/**@brief Process ANT message on ANT master beacon channel.
 *
 *
 * @details   This function handles all events on the master beacon channel.
 *            On EVENT_TX an ANT_BEACON_PAGE message is queued.
 *
 * @param[in] p_ant_event ANT message content.
 */
void master_beacon_process(ant_evt_t * p_ant_evt)
{
	switch(p_ant_evt->event) {
		case EVENT_TX:
			ant_message_send(tx_buffer);
			break;

		default:
			break;
	}
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */

//static uint8_t data_array[UART_RX_BUF_SIZE];
//static uint8_t indexUART = 0;

void uart_event_handle(app_uart_evt_t * p_event)
{
  switch (p_event->evt_type)
  {
    case APP_UART_DATA_READY:
      //app_uart_get(&data_array[indexUART]);
      //indexUART++;
		  //while(app_uart_put('h') != NRF_SUCCESS);
      break;

    case APP_UART_COMMUNICATION_ERROR:
      APP_ERROR_HANDLER(p_event->data.error_communication);
      break;

    case APP_UART_FIFO_ERROR:
      APP_ERROR_HANDLER(p_event->data.error_code);
      break;

    default:
      break;
  }
}
/**@snippet [Handling the data received over UART] */


/* Main function */
int main(void)
{
   uint32_t err_code;

   //--------------- UART INIT ---------------/
   const app_uart_comm_params_t comm_params =
   {
      RX_PIN_NUMBER,
      TX_PIN_NUMBER,
      RTS_PIN_NUMBER,
      CTS_PIN_NUMBER,
      APP_UART_FLOW_CONTROL_DISABLED,
      false,
      UART_BAUDRATE_BAUDRATE_Baud38400
   };
   APP_UART_FIFO_INIT(&comm_params,
                        UART_RX_BUF_SIZE,
                        UART_TX_BUF_SIZE,
                        uart_event_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
   APP_ERROR_CHECK(err_code);
	//------------ END OF UART INIT ------------/

   utils_setup();
   softdevice_setup();
   application_initialize();
   tx_buffer[0] = 0xFC;  // STOP ALL EXPERIMENTS
   //tx_buffer[0] = 0xFD;  // aggregation
   //tx_buffer[0] = 0xFE;  // col_exp
   tx_buffer[1] = 0xFD;
   tx_buffer[2] = 0x64;
   tx_buffer[3] = 0xFF;
   tx_buffer[4] = 0x00;
   tx_buffer[5] = 0xFF;
   tx_buffer[6] = 0xBF;
   tx_buffer[7] = 0xEF;

   // LED for testing
   nrf_gpio_cfg_output(31);


   //--- reset counter average to 1.0 for all robots
   for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
      for (uint8_t j = 0; j < NUM_OF_CHAN; j++) {
         counterAvg[i][j] = 1.0;
      }
   }

   //--- reset the master matrix values to 0xFF
   for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
      for (uint8_t j = 0; j < NUM_OF_CHAN; j++) {
         estimatedProxDataAvg[i][j] = 0xFF;
      }
   }


   /*// for testing purposes only, delete after
   // fill the data so we can read it from the GUI
   for (uint8_t i = 0; i < 9; i++) {
      for (uint8_t j = 0; j < 9; j++) {
         estimatedProxDataAvg[i][j] = j+2+i;
         estimatedBearDataAvg[i][j] = j+1000+i;
      }
   }*/


   uint8_t startSending = 0;
   // Enter main loop
   for (;;)
   {
      err_code = sd_app_evt_wait();
      APP_ERROR_CHECK(err_code);

      uint8_t getData = 0;
      app_uart_get(&getData);
      if (getData == 115) {
         nrf_gpio_pin_set(31);
         startSending = 1;
      }



      // TODO: maybe only send data that has >500 counter value to the host computer
      //       for analysis since it may pick up garbage values



      // we have been given the green light to send data!
      if (startSending) {
         for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
            for (uint8_t j = 0; j < NUM_OF_CHAN; j++) {
#define MIN_COUNTER_TO_SEND   0
               if (counterAvg[i][j] >= MIN_COUNTER_TO_SEND) {
                  // send value of 0xFF if it's proximity value of itself
                  if (i != j) {
                     while(app_uart_put(estimatedProxDataAvg[i][j]) != NRF_SUCCESS);
                  }
                  else {
                     while(app_uart_put(0xFF) != NRF_SUCCESS);
                  }
               }
               else {
                  while(app_uart_put(0xFF) != NRF_SUCCESS);
               }
            }
         }
         for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
            for (uint8_t j = 0; j < NUM_OF_CHAN; j++) {
               while(app_uart_put(estimatedBearDataAvg[i][j] & 0xFF) != NRF_SUCCESS);
               while(app_uart_put((estimatedBearDataAvg[i][j]>>8) & 0xFF) != NRF_SUCCESS);
            }
         }
         startSending = 0;
         nrf_gpio_pin_clear(31);
      }


      //--- display all our data ---/
      /*while(app_uart_put(12) != NRF_SUCCESS);	// delete previous characters
      for (uint8_t j = 0; j < 2; j++) {
         app_trace_log("ROBOT %d DATA:\n\r", j);
         for (uint8_t i = 0; i < NUM_OF_CHAN; i++) {
            // don't print the robot's own readings
            if (i != j) {
               if (estimatedProxDataAvg[j][i] <= 150) {
                  app_trace_log("robot %d:\t%d\t%d\n\r", i, estimatedProxDataAvg[j][i], estimatedBearDataAvg[j][i]);
               }
               else {
                  app_trace_log("robot %d:\tNULL\tNULL\n\r", i);
               }
            }
            else {
               app_trace_log("robot %d:\tNULL\tNULL\n\r", i);
            }
         }
         app_trace_log("\n\n\r");
      }

   	nrf_delay_ms(1000);*/
   }
}
/**
 *@}
 **/
