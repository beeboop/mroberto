//
//  ITG3701.c
//  
//
//  Created by Tyler Colaco on 2015-09-23.
//
//

#include "ITG3701.h"
// Between Stop and Start condition require 1.3 us
//Mention FIFO -> probably not practical due to RT nature of WiSaR
//XOUT_H = 0x43
//XOUT_L = 0x44
//YOUT_H = 0x45
//YOUT_L = 0x46
//ZOUT_H = 0x47
//ZOUT_L = 0x48
//GYRO_CONFIG = 0x1B
// Checking the Data Ready interrupt. 


void itg3701Init(void)
{
	uint8_t data[2] = {CONFIG, 0x00};
	//Set Sampling Rate in Config Register
	//Disable FIFO Mode
	twi_master_transfer(GYR_ADDR_W, data, 2, 1);	
	nrf_delay_ms(5);	
	
	//Configure Interupt
	//Clear Interupt on Read
	data[0] = INT_CF;
	data[1] = 0x10;
	twi_master_transfer(GYR_ADDR_W, data, 2, 1);	
	nrf_delay_ms(5);
	
	//Enable Interupt
	//Enable Data_Rdy Interupt
	data[0] = INT_EN;
	data[1] = 0x01;
	twi_master_transfer(GYR_ADDR_W, data, 2, 1);	
	
	return;
}


void itg3701ClearInt(void)
{
	uint8_t data[2] = {INT_ST, 0};
	//Clear any pending interupts
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);	

	return;
}


int itg3701Status(void)
{
	uint8_t data[2] = {INT_ST, 0};

	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	
	return (data[0] & 0x01);
}


int16_t itg3701Readx(void)
{
	//itg3701ClearInt();
	while(!itg3701Status());	// wait for new set of data
	uint8_t xl = 0, xh = 0;		//define the MSB and LSB

	// transmit to device 0x0E
	// x MSB reg
	uint8_t data[2] = {XOUT_L, 0};
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	xl = data[0];			// receive the byte
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	data[0] = XOUT_H;
	
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	xh = data[0];			// receive the byte
	
	int16_t xout = (xl | (xh << 8));	//concatenate the MSB and LSB
	
	//return ((float)xout * (1.0 / (-1481 + 2249)));
	return xout;
}


int16_t itg3701Ready(void)
{
	//itg3701ClearInt();
	while(!itg3701Status());	// wait for new set of data
	
	uint8_t yl = 0, yh = 0;		//define the MSB and LSB
	
	uint8_t data[2] = {YOUT_L, 0};
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);

	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	yl = data[0];			// receive the byte
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	data[0] = YOUT_H;
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	yh = data[0];			// receive the byte
	
	int16_t yout = (yl | (yh << 8));	//concatenate the MSB and LSB
	
	//return ((float)yout * (1.0 / (1679 - 875)));
	return yout;
}


float itg3701Readz(void)
{
	//itg3701ClearInt();
	while(!itg3701Status());	// wait for new set of data
	
	uint8_t zl = 0, zh = 0;		//define the MSB and LSB
	
	uint8_t data[2] = {ZOUT_L, 0};
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);

	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	zl = data[0];			// receive the byte
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	data[0] = ZOUT_H;
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	zh = data[0];			// receive the byte
	
	int16_t zout = (zl | (zh << 8));	//concatenate the MSB and LSB
	
	return (((float)zout)/FS_SEL_0);
}

