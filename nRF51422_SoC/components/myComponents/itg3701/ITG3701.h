//
//  ITG3701.h
//  
//
//  Created by Tyler Colaco on 2015-09-23.
//
//

#ifndef ITG3701_h
#define ITG3701_h

#include <stdio.h>
#include <math.h>
#include "twi_master.h"
#include "twi_master_config.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"

#define GYR_ADDR_W  0xD0	// 8-bit WRITE address for the ITG3701
#define GYR_ADDR_R  0xD1	// 8-bit READ address for the ITG3701

#define CONFIG 0x1A			// 8-bit address for Configuration Register
#define INT_CF 0x37			// 8-bit address for Interupt Config Register
#define INT_EN 0x38			// 8-bit address for Interupt Enable Register
#define INT_ST 0x3A			// 8-bit address for Interupt Status Register
#define XOUT_H 0x43			// 8-bit address for upper byte of X Sensor
#define XOUT_L 0x44			// 8-bit address for lower byte of X Sensor
#define YOUT_H 0x45			// 8-bit address for upper byte of Y Sensor
#define YOUT_L 0x46			// 8-bit address for lower byte of Y Sensor
#define ZOUT_H 0x47			// 8-bit address for upper byte of Z Sensor
#define ZOUT_L 0x48			// 8-bit address for lower byte of Z Sensor
#define GYRO_CONFIG 0x1B
 
#define FS_SEL_0	65.5	// LSB per deg/s for 500 deg/s res


#define PI 3.14159265359

void itg3701Init(void);
void itg3701ClearInt(void);
int itg3701Status(void);
int16_t itg3701Readx(void);
int16_t itg3701Ready(void);
float itg3701Readz(void);






#endif /* ITG3701_h */
