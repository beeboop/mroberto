/**********************************************************************************************
*
* swarmModule.h						Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#ifndef SWARM_MODULE_h
#define SWARM_MODULE_h

#include <stdio.h>
#include <math.h>
#include "twi_master.h"
#include "twi_master_config.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "app_pwm.h"


#define SWARM_ADDR_W	0xA0	// 8-bit WRITE address for swarm module
#define SWARM_ADDR_R	0xA1	// 8-bit READ address for swarm module

#define EMITTER_ADDR_W	0xB0	// 8-bit WRITE address for swarm emitter module
#define EMITTER_ADDR_R	0xB1	// 8-bit READ address for swarm emitter module


// we have 6 IR detectors
#define NUM_OF_IRSENS		6
// total channels on the swarm module
#define NUM_OF_CHANNELS		9
// max buffer size of all proximities
#define BUFFER_SIZE_PROX	(NUM_OF_IRSENS*NUM_OF_CHANNELS)

#define WAIT_MS_UPDATE		320.0	// 320 ms
#define FREQ_AVG			30.0

/* reg hex values for TWI on swarm module */
#define RGBLED	0x01
#define IRFREQ	0x02
#define IRONOFF	0x03
#define STARTP	0x04
#define GETPROX	0x05
#define SETCHAN	0x06
#define GETDATA	0x07
#define GETSCAL	0x08
#define GETOFFS	0x09


#define PI 3.14159265359


/*********************************************************************************************
*
* Swarm Module Initialization
*
**********************************************************************************************
*
* Description:
*	Initializes the swarm module for either calibration mode or normal operation mode.
*
**********************************************************************************************
*
* Varaibles:
*	moduleMode	->	sets the swarm module into calibration mode (0) or normal operation
*					mode (1).
*
*********************************************************************************************/
void swarmModuleInit(uint8_t moduleMode);

void setLEDs(uint8_t redLED, uint8_t greLED, uint8_t bluLED);

void setIR_ON_OFF(unsigned short stateIR);

void setModulationFrequency(unsigned short freqValue);

void setEmitChannel(uint8_t channelNum);

void startGetProximities(uint8_t getOurs);

void getAllProximities(uint8_t allProximities[NUM_OF_IRSENS][NUM_OF_CHANNELS]);



//#define MIN_DIS_MEASURE		D_FAR	// ignores all measurements above D_FAR
#define MIN_DIS_MEASURE		115
#define MAX_REL_DIS			255		// mm units

void getAllData(uint8_t *proximityData, uint16_t *bearingData);


void getAvgMeasurement(uint8_t *proximityData, uint16_t *bearingData);



void getAvgData(uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData);

/*
void getAvgMeasurementAndMoveForward(uint8_t *proximityData, uint16_t *bearingData,
	app_pwm_t const * const PWM1, app_pwm_t const * const PWM2);*/

void getOffsets(int16_t *offsetGyro, int16_t *offsetAccel);

#endif /* SWARM_MODULE_h */

