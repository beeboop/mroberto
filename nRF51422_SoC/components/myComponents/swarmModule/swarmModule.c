//
//  swarmModule.c
//  
//
//  Created by Justin Y. Kim on 04-04-2016
//
//


#include "swarmModule.h"


void swarmModuleInit(uint8_t moduleMode)
{
	uint8_t data[3];
	// enter calibration mode
	if (moduleMode == 0) {
		/** SETTING UP SWARM EMITTER MODULE **/
		// turn ON IR emitters
		data[0] = IRONOFF;
		data[1] = 0x01;
		twi_master_transfer(EMITTER_ADDR_W, data, 2, 1);
		
		data[0] = IRFREQ;
		data[1] = 0x06;
		data[2] = 0xF4;
		twi_master_transfer(EMITTER_ADDR_W, data, 3, 1);
	}
	// enter normal operation mode
	else if (moduleMode == 1) {
		
	}
	
	return;
}


void setLEDs(uint8_t redLED, uint8_t greLED, uint8_t bluLED)
{
	// turn ON/OFF RGB LED
	uint8_t data[2];
	data[0] = RGBLED;
	data[1] = ((redLED << 2) & 0b00000100) | (greLED & 0b00000001) | ((bluLED << 1) & 0b00000010);
	twi_master_transfer(SWARM_ADDR_W, data, 2, 1);
}


void setIR_ON_OFF(unsigned short stateIR)
{
	// turn ON IR emitters
	uint8_t data[2];
	data[0] = IRONOFF;
	data[1] = stateIR & 0x01;
	twi_master_transfer(EMITTER_ADDR_W, data, 2, 1);
}


void setModulationFrequency(unsigned short freqValue)
{
	// set PWM freq to freqValue
	uint8_t data[3] = {IRFREQ, (uint8_t)(0xFF & (freqValue >> 8)), (uint8_t)(0xFF & (freqValue))};
	twi_master_transfer(EMITTER_ADDR_W, data, 3, 1);
	
	return;
}


void setEmitChannel(uint8_t channelNum)
{
	// set PWM freq to freqValue
	uint8_t data[2] = {SETCHAN, channelNum};
	twi_master_transfer(SWARM_ADDR_W, data, 2, 1);
	
	return;
}


void startGetProximities(uint8_t getOurs)
{
	// turn ON IR emitters
	uint8_t data[2];
	data[0] = STARTP;
	data[1] = 0x01 | (getOurs << 1);
	twi_master_transfer(SWARM_ADDR_W, data, 2, 1);
}


void getAllProximities(uint8_t allProximities[NUM_OF_IRSENS][NUM_OF_CHANNELS])
{
	uint8_t data[2] = {GETPROX, 0x00};
	uint8_t proxValues[BUFFER_SIZE_PROX];
	twi_master_transfer(SWARM_ADDR_W, data, 1, 0);
	twi_master_transfer(SWARM_ADDR_R, proxValues, BUFFER_SIZE_PROX, 1);

	// order the proximity amplitudes
	short counter = 0;
	for (short i = 0; i < NUM_OF_IRSENS; i++) {
		for (short j = 0; j < NUM_OF_CHANNELS; j++) {
			allProximities[i][j] = proxValues[counter];
			counter++;
			/*for (short k = 0; k < 4; k++) {
				// gets high 8 bits
				if (k == 0) {
					allProximities[i][j] = ((uint32_t)proxValues[counter] << 24);
				}
				else if (k == 1) {
					allProximities[i][j] |= ((uint32_t)proxValues[counter] << 16);
				}
				else if (k == 2) {
					allProximities[i][j] |= ((uint32_t)proxValues[counter] << 8);
				}
				// gets low 8 bits
				else {
					allProximities[i][j] |= (uint32_t)proxValues[counter];
				}
				counter++;
			}*/
		}
	}
}


void getAllData(uint8_t *proximityData, uint16_t *bearingData)
{
	uint8_t data[2] = {GETDATA, 0x00};
	uint8_t buffer[NUM_OF_CHANNELS * 3];
	twi_master_transfer(SWARM_ADDR_W, data, 1, 0);
	twi_master_transfer(SWARM_ADDR_R, buffer, (NUM_OF_CHANNELS * 3), 1);
	
	// order the bearings
	short counter = 0;
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		for (short j = 0; j < 2; j++) {
			// gets high 8 bits
			if (j % 2 == 0) {
				bearingData[i] = (uint16_t)(buffer[counter] << 8);
			}			
			// gets low 8 bits
			else {
				bearingData[i] |= (uint16_t)buffer[counter];
			}
			counter++;
		}
	}
	// order the proximities
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityData[i] = (uint8_t)buffer[counter];
		counter++;
	}
}


void getAvgMeasurement(uint8_t *proximityData, uint16_t *bearingData)
{
	unsigned int proximityDataAvg[NUM_OF_CHANNELS];
	int bearingDataAvg[NUM_OF_CHANNELS];
	// set all avg values to start off at value of 0
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityDataAvg[i] = 0;
		bearingDataAvg[i] = 0;
	}
	uint8_t proximityDataBuffer[NUM_OF_CHANNELS];
	uint16_t bearingDataBuffer[NUM_OF_CHANNELS];
	
	// sum up all the measurement values
	for (short i = 0; i < FREQ_AVG; i++) {
		// start the measurement
		startGetProximities(0);
		// wait 340 ms for the measurement to finish
		nrf_delay_ms(WAIT_MS_UPDATE);
		// get our proximities and bearings data
		getAllData(proximityDataBuffer, bearingDataBuffer);
		
		for (short j = 0; j < NUM_OF_CHANNELS; j++) {
			proximityDataAvg[j] += proximityDataBuffer[j];
			//--- take care of corner case when transistioning from 0 to 360 deg ---/
			// only go through corner case check if not first time
			if (i > 0) {
				if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) > 315) {
					bearingDataAvg[j] += bearingDataBuffer[j] + 360.0;
				}
				else if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) < -315) {
					bearingDataAvg[j] += bearingDataBuffer[j] - 360.0;
				}
				// not within corner case
				else {
					bearingDataAvg[j] += bearingDataBuffer[j];
				}
			}
			else {
				bearingDataAvg[j] += bearingDataBuffer[j];
			}
		}
	}
	
	// average out all the measurments and assign the avg prox/bearings
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityData[i] = (unsigned int)((float)proximityDataAvg[i] / FREQ_AVG);
		bearingDataAvg[i] = (int)((float)bearingDataAvg[i] / FREQ_AVG);
		// ensure bearing is not negative
		if (bearingDataAvg[i] < 0) {
			bearingDataAvg[i] += 360;
		}
		bearingData[i] = bearingDataAvg[i];
	}
	
	return;
}


void getAvgData(uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData)
{
	unsigned int proximityDataAvg[NUM_OF_CHANNELS];
	float dataCounter[NUM_OF_CHANNELS];
	int bearingDataAvg[NUM_OF_CHANNELS];
	// set all avg values to start off at value of 0
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityDataAvg[i] = 0;
		bearingDataAvg[i] = 0;
		dataCounter[i] = 0;
	}
	uint8_t proximityDataBuffer[NUM_OF_CHANNELS];
	uint16_t bearingDataBuffer[NUM_OF_CHANNELS];
	
	// sum up all the measurement values
	for (short i = 0; i < FREQ_AVG; i++) {
		// start the measurement
		startGetProximities(0);
		// wait 330 ms for the measurement to finish
		nrf_delay_ms(WAIT_MS_UPDATE);
		// get our proximities and bearings data
		getAllData(proximityDataBuffer, bearingDataBuffer);
		
		for (short j = 0; j < NUM_OF_CHANNELS; j++) {
			// only add it in if distance is less than MIN_DIS_MEASURE
			if (proximityDataBuffer[j] <= MIN_DIS_MEASURE) {
				proximityDataAvg[j] += proximityDataBuffer[j];
				//--- take care of corner case when transistioning from 0 to 360 deg ---/
				// only go through corner case check if not first time
				if (i > 0) {
					if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) > 330) {
						bearingDataAvg[j] += bearingDataBuffer[j] + 360.0;
					}
					else if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) < -330) {
						bearingDataAvg[j] += bearingDataBuffer[j] - 360.0;
					}
					// not within corner case
					else {
						bearingDataAvg[j] += bearingDataBuffer[j];
					}
				}
				else {
					bearingDataAvg[j] += bearingDataBuffer[j];
				}
				// increment our avg counter
				dataCounter[j]++;
			}
		}
	}
	
	// average out all the measurments and assign the avg prox/bearings
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		// only calculate the ones with counters > 0
		if (dataCounter[i] > 0) {
			proximityData[i] = (unsigned int)((float)proximityDataAvg[i] / dataCounter[i]);
			bearingDataAvg[i] = (int)((float)bearingDataAvg[i] / dataCounter[i]);
			// ensure bearing is not negative
			if (bearingDataAvg[i] < 0) {
				bearingDataAvg[i] += 360;
			}
			bearingData[i] = bearingDataAvg[i];
		}
		// no robots were seen, set rel distance to MAX possible value
		else {
			proximityData[i] = MAX_REL_DIS;
		}
	}
	
	return;
}





/*
void getAvgMeasurementAndMoveForward(uint8_t *proximityData, uint16_t *bearingData,
	app_pwm_t const * const PWM1, app_pwm_t const * const PWM2)
{
	unsigned int proximityDataAvg[NUM_OF_CHANNELS];
	int bearingDataAvg[NUM_OF_CHANNELS];
	// set all avg values to start off at value of 0
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityDataAvg[i] = 0;
		bearingDataAvg[i] = 0;
	}
	uint8_t proximityDataBuffer[NUM_OF_CHANNELS];
	uint16_t bearingDataBuffer[NUM_OF_CHANNELS];
	
	// sum up all the measurement values
	for (short i = 0; i < FREQ_AVG; i++) {
		// start the measurement
		startGetProximities(0);
		// wait 340 ms for the measurement to finish
		moveForward(WAIT_MS_UPDATE, PWM1, PWM2);
		// get our proximities and bearings data
		getAllData(proximityDataBuffer, bearingDataBuffer);
		
		for (short j = 0; j < NUM_OF_CHANNELS; j++) {
			proximityDataAvg[j] += proximityDataBuffer[j];
			//--- take care of corner case when transistioning from 0 to 360 deg ---/
			// only go through corner case check if not first time
			if (i > 0) {
				if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) > 315) {
					bearingDataAvg[j] += bearingDataBuffer[j] + 360.0;
				}
				else if ((((float)bearingDataAvg[j]/((float)i+1.0)) - bearingDataBuffer[j]) < -315) {
					bearingDataAvg[j] += bearingDataBuffer[j] - 360.0;
				}
				// not within corner case
				else {
					bearingDataAvg[j] += bearingDataBuffer[j];
				}
			}
			else {
				bearingDataAvg[j] += bearingDataBuffer[j];
			}
		}
	}
	
	// average out all the measurments and assign the avg prox/bearings
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		proximityData[i] = (unsigned int)((float)proximityDataAvg[i] / FREQ_AVG);
		bearingDataAvg[i] = (int)((float)bearingDataAvg[i] / FREQ_AVG);
		// ensure bearing is not negative
		if (bearingDataAvg[i] < 0) {
			bearingDataAvg[i] += 360;
		}
		bearingData[i] = bearingDataAvg[i];
	}
	
	return;
}
*/

void getOffsets(int16_t *offsetGyro, int16_t *offsetAccel)
{
	uint8_t data[2] = {GETOFFS, 0x00};
	uint8_t buffer[4];
	twi_master_transfer(SWARM_ADDR_W, data, 1, 0);
	twi_master_transfer(SWARM_ADDR_R, buffer, 4, 1);
	
	*offsetGyro = (int16_t)buffer[0] | ((int16_t)buffer[1] << 8);
	*offsetAccel = (int16_t)buffer[2] | ((int16_t)buffer[3] << 8);
}

