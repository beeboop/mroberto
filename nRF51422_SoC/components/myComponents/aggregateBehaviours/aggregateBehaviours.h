/**********************************************************************************************
*
* aggregateBehaviours.h				Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#ifndef AGGREGATE_BEHAVIOURS_H
#define AGGREGATE_BEHAVIOURS_H


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "swarmModule.h"
#include "motorSchema.h"



#define D_LIMIT		30.0	// 30 mm
#define D_FAR		90.0	// 90 mm
#define ATT_SCALE	1.2
void approachRobots(float *v_x, float *v_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData);



#endif /* AGGREGATE_BEHAVIOURS_H */


