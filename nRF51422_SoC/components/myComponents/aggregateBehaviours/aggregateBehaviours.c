

#include "aggregateBehaviours.h"



// call function as adjustDistance(&v_x, &v_y, ...)

void approachRobots(float *v_x, float *v_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData)
{
	// initialize all variables
	*v_x = 0;
	*v_y = 0;
	float magMovement = 0;
	
	for (unsigned short counter = 0; counter < NUM_OF_CHANNELS; counter++) {
		//--- ignore if less than D_LIMIT or greater than D_FAR ---/
		if (proximityData[counter] <= D_LIMIT) {
			// too close! go into 'Wait' state!
			*v_x = 0;
			*v_y = 0;
			break;
		}
		else if (proximityData[counter] > D_FAR) {
			// do nothing...
		}
		else {
			// if the values are from our channel, ignore
			if (counter == myChannel) {
				magMovement = 0;
			}
			else {
				magMovement = ATT_SCALE * (((float)proximityData[counter]) - D_LIMIT);
			}
			// sum up our x and y vectors
			*v_x += magMovement * cos((float)bearingData[counter]*(PI/180.0));
			*v_y += magMovement * sin((float)bearingData[counter]*(PI/180.0));
		}
	}
}




