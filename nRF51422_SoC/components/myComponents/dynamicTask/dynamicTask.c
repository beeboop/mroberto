/**********************************************************************************************
*
* dynamicTask.c				Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: June 22, 2016
*
*/


#include "dynamicTask.h"



void trackAndBroadcastSmallest(uint16_t mSeconds)
{
	lowestID = MY_CHANNEL;
	
	// set our message
	tx_buffer[0] = 0xF0;
	tx_buffer[1] = 0xF3;
	tx_buffer[2] = lowestID;

	// turn on ANT broadcaster
	uint32_t err_code = sd_ant_channel_open(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	// wait for whatever set period
	nrf_delay_ms(mSeconds);
	
	// turn off ANT broadcaster
	err_code = sd_ant_channel_close(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	return;
}


void trackAndBroadcastSmallest_TASK(uint16_t mSeconds)
{
	lowestID = MY_CHANNEL;
	
	// set our message
	tx_buffer[0] = 0xF2;
	tx_buffer[1] = 0xF3;
	tx_buffer[2] = lowestID;
	tx_buffer[3] = currentStage;
	tx_buffer[4] = 0xFF;
	tx_buffer[5] = 0xFF;
	tx_buffer[6] = 0xFF;
	tx_buffer[7] = 0xFF;

	// turn on ANT broadcaster
	uint32_t err_code = sd_ant_channel_open(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	// wait for whatever set period
	nrf_delay_ms(mSeconds);
	
	// clearing out the message buffer before closing the channel
	tx_buffer[0] = 0xFF;
	tx_buffer[1] = 0xFF;
	tx_buffer[2] = 0xFF;
	tx_buffer[3] = 0xFF;
	tx_buffer[4] = 0xFF;
	tx_buffer[5] = 0xFF;
	tx_buffer[6] = 0xFF;
	tx_buffer[7] = 0xFF;
	
	// wait for 1 second of clearing last message
	nrf_delay_ms(1000);
	
	// turn off ANT broadcaster
	err_code = sd_ant_channel_close(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	return;
}


void broadcastNewStage(uint16_t mSeconds)
{
	lowestID = MY_CHANNEL;
	
	// set our message
	tx_buffer[0] = 0xF2;
	tx_buffer[1] = 0xF4;
	tx_buffer[2] = 0xFF;
	tx_buffer[3] = currentStage;
	tx_buffer[4] = 0xFF;
	tx_buffer[5] = 0xFF;
	tx_buffer[6] = 0xFF;
	tx_buffer[7] = 0xFF;

	// turn on ANT broadcaster
	uint32_t err_code = sd_ant_channel_open(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	// wait for whatever set period
	nrf_delay_ms(mSeconds);
	
	// turn off ANT broadcaster
	err_code = sd_ant_channel_close(ANT_MS_CHANNEL_NUMBER);
    APP_ERROR_CHECK(err_code);
	
	return;
}

