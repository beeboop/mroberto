/**********************************************************************************************
*
* dynamicTask.h				Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: June 22, 2016
*
*/


#ifndef DYNAMIC_TASK_H
#define DYNAMIC_TASK_H


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "swarmModule.h"
#include "ant_stuffz.h"



volatile uint8_t lowestID;
volatile uint8_t levelCounter;
volatile uint8_t highestLevel;
volatile uint8_t internalState;
volatile uint8_t currentStage;

void trackAndBroadcastSmallest(uint16_t mSeconds);


void trackAndBroadcastSmallest_TASK(uint16_t mSeconds);


void broadcastNewStage(uint16_t mSeconds);


#endif /* DYNAMIC_TASK_H */

