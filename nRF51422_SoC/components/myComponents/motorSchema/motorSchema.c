/**********************************************************************************************
*
* motorSchema.c						Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/

#include "motorSchema.h"



void turnUsingGyro(int16_t turnAngle, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2)
{
	if (turnAngle > 180) {
		turnAngle = turnAngle - 360;
	}

	double currentTheta = 0, curTheta_dot = 0;
	uint16_t motorMAG = MOTOR_MAG;

	while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);	// left backward
	while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);	// left forward
	while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);	// right forward
	while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);	// right backward
	nrf_delay_ms(10);

	while (abs(currentTheta) <= abs(turnAngle)) {
		motorMAG = (uint16_t)(MOTOR_MAG + K_P_MOTOR * (abs((float)turnAngle) - abs(currentTheta)));

#if (MY_CHANNEL == 2) || (MY_CHANNEL == 6)
		if (motorMAG < MOTOR_MAG_MIN) {
			motorMAG = MOTOR_MAG_MIN;
		}
#endif

		//motorMAG = MOTOR_MAG;

		//--- turn left ---/
		if (turnAngle < 0) {
			while (app_pwm_channel_duty_set(PWM1, 1, motorMAG) == NRF_ERROR_BUSY);	// left forward
			while (app_pwm_channel_duty_set(PWM2, 1, motorMAG) == NRF_ERROR_BUSY);	// right backward
		}
		//--- turn right ---/
		else if (turnAngle > 0) {
			while (app_pwm_channel_duty_set(PWM1, 0, motorMAG) == NRF_ERROR_BUSY);	// left backward
			while (app_pwm_channel_duty_set(PWM2, 0, motorMAG) == NRF_ERROR_BUSY);	// right forward
		}

		// add up our total turned theta
		curTheta_dot = mpu6500Readz_GYRO(0);
		currentTheta += curTheta_dot * T_PERIOD_S;
		nrf_delay_ms(T_PERIOD_MS - T_PERIOD_MS_OFFSET);
	}
	// stop all the motors
	while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);	// left backward
	while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);	// left forward
	while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);	// right forward
	while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);	// right backward
}


void turnWithRobot(int16_t turnAngle, uint8_t robot2TurnTo, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2)
{
	// ensure turn angle is within range of 0 to 360 degrees
	if (turnAngle < 0) {
		turnAngle += 360;
	}

	// initialize all required variables
	uint8_t		proximityData[NUM_OF_CHANNELS];
	uint16_t	bearingData[NUM_OF_CHANNELS];
	int16_t		angleDiff = 0;

	// go into our turning loopity doop
	//do {
		// get our measurements of other nearby robots
		getAvgMeasurement(proximityData, bearingData);

		// see if we have to turn left or right
		angleDiff = turnAngle - (int16_t)bearingData[robot2TurnTo];
		// check for corner cases
		if (angleDiff < -180) {
			angleDiff += 360;
		}
		else if (angleDiff > 180) {
			angleDiff -= 360;
		}
		// turn either left or right depending on the angle sign
		if (angleDiff < 0) {
			while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);		// left backward
			while (app_pwm_channel_duty_set(PWM1, 1, 100) == NRF_ERROR_BUSY);	// left forward
			while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);		// right forward
			while (app_pwm_channel_duty_set(PWM2, 1, 100) == NRF_ERROR_BUSY);	// right backward

			while(angleDiff < 0) {
				getAvgMeasurement(proximityData, bearingData);
				angleDiff = turnAngle - (int16_t)bearingData[robot2TurnTo];
				if (angleDiff < -180) {
					angleDiff += 360;
				}
				else if (angleDiff > 180) {
					angleDiff -= 360;
				}
			}
		}
		else if (angleDiff > 0) {
			while (app_pwm_channel_duty_set(PWM1, 0, 100) == NRF_ERROR_BUSY);	// left backward
			while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);		// left forward
			while (app_pwm_channel_duty_set(PWM2, 0, 100) == NRF_ERROR_BUSY);	// right forward
			while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);		// right backward

			while(angleDiff > 0) {
				getAvgMeasurement(proximityData, bearingData);
				angleDiff = turnAngle - (int16_t)bearingData[robot2TurnTo];
				if (angleDiff < -180) {
					angleDiff += 360;
				}
				else if (angleDiff > 180) {
					angleDiff -= 360;
				}
			}
		}
		//nrf_delay_ms(100);
	//} while(abs(angleDiff) > WITHIN_RANGE_DEG);
	// stop all motors!
	while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);	// left backward
	while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);	// left forward
	while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);	// right forward
	while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);	// right backward
}


void moveForward(uint16_t msSeconds, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2)
{
	float curTheta = 0.0;
	uint16_t counterMS = 0;

	while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);	// left backward
	while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);	// left forward
	while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);	// right forward
	while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);	// right backward

	while (counterMS <= msSeconds) {


#if (MY_CHANNEL == 2)
		while (app_pwm_channel_duty_set(PWM1, 1, 95 + K_GAIN * curTheta) == NRF_ERROR_BUSY);	// left forward
		while (app_pwm_channel_duty_set(PWM2, 0, 95 - K_GAIN * curTheta) == NRF_ERROR_BUSY);	// right forward
#elif  (MY_CHANNEL == 6)
		while (app_pwm_channel_duty_set(PWM1, 1, 95 + K_GAIN * curTheta) == NRF_ERROR_BUSY);	// left forward
		while (app_pwm_channel_duty_set(PWM2, 0, 95 - K_GAIN * curTheta) == NRF_ERROR_BUSY);
#else
		while (app_pwm_channel_duty_set(PWM1, 1, FOR_MAG + K_GAIN * curTheta) == NRF_ERROR_BUSY);	// left forward
		while (app_pwm_channel_duty_set(PWM2, 0, FOR_MAG - K_GAIN * curTheta) == NRF_ERROR_BUSY);	// right forward
#endif

#if ((MY_CHANNEL != 1) && (MY_CHANNEL != 0))
		curTheta += mpu6500Readz_GYRO(0) * T_PERIOD_S;
#endif
		nrf_delay_ms(T_PERIOD_MS - T_PERIOD_MS_OFFSET);

		counterMS += T_PERIOD_MS - T_PERIOD_MS_OFFSET;
	}
	while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);	// left backward
	while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);	// left forward
	while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);	// right forward
	while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);	// right backward
}





// control gain parameters
#define K_P				4.0//1.05
#define K_D				0.0//1.5
#define K_I				0.5//0.14
#define B_PARAMETER		0.5
#define EPSILON			0.1
#define	MAX_SPEED		190
// robot parameters
#define WHEEL_R			0.6571429		// 0.8 mm / (1.2173913 mm/pixel)
#define AXLE_D			13.142857		// 16 mm / (1.2173913 mm/pixel)

float v1 = 0, v2 = 0, v1Past = 0, v2Past = 0;
float nextX_dot = 0, nextY_dot = 0, curAccel = 0;
float currentX = 0, currentY = 0, currentX_dot = 0, currentY_dot = 0;
float pastX = 0, pastY = 0;
float currentTheta = 0, curTheta_dot = 0;
float motorW_L = 0, motorW_R = 0;
float u_1_next = 0, u_2_next = 0;
unsigned short counterPause = 0;

void moveForwardNow(float nextX, float nextY, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2)
{
	currentX_dot = 0;
	currentY_dot = 0;
	currentX = 0;
	pastX = 0;
	currentY = 0;
	pastY = 0;
	currentTheta = 0;
	v1Past = 0;
	v2Past = 0;
	//counterPause = (WAIT_MS_UPDATE / T_PERIOD_MS) * FREQ_AVG;
	counterPause = 80;

	while ((counterPause > 0) && ((abs(nextX) > 0) || (abs(nextY) > 0))) {
		// calculate v1 and v2 in pixel units
		v1 = (K_P * (nextX - currentX)) + //(K_D * (x_dot_coord[timeCounter] - robotProfile::currentX_dot)) +
		K_I * (v1Past + (nextX - (currentX + currentX_dot + (0.25 * v1Past)) + (nextX - currentX) / 2.0) * T_PERIOD_S);
		v2 = (K_P * (nextY - currentY)) + //(K_D * (y_dot_coord[timeCounter] - robotProfile::currentY_dot)) +
		K_I * (v2Past + (nextY - (currentY + currentY_dot + (0.25 * v2Past)) + (nextY - currentY) / 2.0) * T_PERIOD_S);

		// save our past v values
		v1Past = v1;
		v2Past = v2;

		// calculate the next x and y velocity to find next u1 and u2
		nextX_dot = currentX_dot + B_PARAMETER * v1;
		nextY_dot = currentY_dot + B_PARAMETER * v2;

		currentX_dot = currentX - pastX;
		currentY_dot = currentY - pastY;

		pastX = currentX;
		pastY = currentY;

		// calculate next u1 and u2
		u_1_next = sqrt((nextY_dot * nextY_dot) + (nextX_dot * nextX_dot));
		u_2_next = ((v2 * cos(currentTheta * (PI/180.0))) - (v1 * sin(currentTheta * (PI/180.0)))) / (u_1_next + EPSILON);

		// calculate the angular velocity for left and right wheel
		motorW_L = ((u_1_next - 2.0 * AXLE_D * u_2_next) / WHEEL_R);
		motorW_R = ((u_1_next + 2.0 * AXLE_D * u_2_next) / WHEEL_R);

		// ensure below max speed
		if (motorW_L > MAX_SPEED) {
			motorW_L = MAX_SPEED;
		}
		else if (motorW_L < -MAX_SPEED) {
			motorW_L = -MAX_SPEED;
		}
		if (motorW_R > MAX_SPEED) {
			motorW_R = MAX_SPEED;
		}
		else if (motorW_R < -MAX_SPEED) {
			motorW_R = -MAX_SPEED;
		}

		// check for signs of the angular velocities
		if (motorW_L >= 0) {
			while (app_pwm_channel_duty_set(PWM1, 0, 0) == NRF_ERROR_BUSY);
			while (app_pwm_channel_duty_set(PWM1, 1, abs(motorW_L)) == NRF_ERROR_BUSY);
		}
		else if (motorW_L < 0) {
			while (app_pwm_channel_duty_set(PWM1, 0, abs(motorW_L)) == NRF_ERROR_BUSY);
			while (app_pwm_channel_duty_set(PWM1, 1, 0) == NRF_ERROR_BUSY);
		}

		if (motorW_R >= 0) {
			while (app_pwm_channel_duty_set(PWM2, 0, abs(motorW_R)) == NRF_ERROR_BUSY);
			while (app_pwm_channel_duty_set(PWM2, 1, 0) == NRF_ERROR_BUSY);
		}
		else if (motorW_R < 0) {
			while (app_pwm_channel_duty_set(PWM2, 0, 0) == NRF_ERROR_BUSY);
			while (app_pwm_channel_duty_set(PWM2, 1, abs(motorW_R)) == NRF_ERROR_BUSY);
		}

		// add up our total turned theta
		curTheta_dot = mpu6500Readz_GYRO(0);
		curAccel = mpu6500Readx_ACCEL(0);
		currentTheta += curTheta_dot * T_PERIOD_S;
		currentX += curAccel*T_PERIOD_S*T_PERIOD_S * cos(currentTheta);
		currentY += curAccel*T_PERIOD_S*T_PERIOD_S * sin(currentTheta);
		nrf_delay_ms(T_PERIOD_MS - T_PERIOD_MS_OFFSET);
		// decrement our timer
		counterPause--;
	}
}
