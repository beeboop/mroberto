/**********************************************************************************************
*
* motorSchema.h						Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#ifndef MOTOR_SCHEMA_H
#define MOTOR_SCHEMA_H


#include <stdio.h>
#include <stdlib.h>
#include "swarmModule.h"
#include "mpu6500.h"
#include "app_pwm.h"
#include "aggregateBehaviours.h"
#include "ant_stuffz.h"

#if (MY_CHANNEL == 0)
	#define MOTOR_MAG		160.0
	#define FOR_MAG	185.0
#elif (MY_CHANNEL == 2)
	#define MOTOR_MAG		22.0
	#define MOTOR_MAG_MIN	82.0
#elif (MY_CHANNEL == 6)
	#define MOTOR_MAG		40.0
	#define MOTOR_MAG_MIN	125.0
#elif (MY_CHANNEL == 4)
	#define MOTOR_MAG		130.0	// for non-col-explore
	//#define FOR_MAG			145.0	// for non-col-explore
	//#define MOTOR_MAG		155.0	// for col-explore
	#define FOR_MAG			150.0	// for col-explore
#elif (MY_CHANNEL == 7)
		#define MOTOR_MAG		150.0	// for non-col-explore
		//#define FOR_MAG			125.0	// for non-col-explore
		//#define MOTOR_MAG		155.0	// for col-explore
		#define FOR_MAG			176.0	// for col-explore
#elif (MY_CHANNEL == 8)
	#define MOTOR_MAG		93.0	// for non-col-explore
	//#define FOR_MAG			125.0	// for non-col-explore
	//#define MOTOR_MAG		155.0	// for col-explore
	#define FOR_MAG			118.0	// for col-explore
#else
	#define MOTOR_MAG		150.0		// for non-col-explore
	//#define FOR_MAG			165.0		// for non-col-explore
	//#define MOTOR_MAG		180.0		// for collective exploration
	#define FOR_MAG			170.0		// for collective exploration
#endif


#define K_P_MOTOR				0.6

#if ((MY_CHANNEL == 1) || (MY_CHANNEL == 0))
	#define T_PERIOD_MS				50.0	// 50 ms
#else
	#define T_PERIOD_MS				10.0	// 10 ms
#endif

#define T_PERIOD_MS_OFFSET		3.0		// 3 ms
#define T_PERIOD_S				T_PERIOD_MS/1000.0
void turnUsingGyro(int16_t turnAngle, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2);


#define WITHIN_RANGE_DEG		10.0		// 5 degrees of error allowed
void turnWithRobot(int16_t turnAngle, uint8_t robot2TurnTo, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2);


#define K_GAIN					10.0
void moveForward(uint16_t msSeconds, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2);


void moveForwardNow(float nextX, float nextY, app_pwm_t const * const PWM1, app_pwm_t const * const PWM2);



#endif /* MOTOR_SCHEMA_H */
