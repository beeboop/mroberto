/**********************************************************************************************
*
* mpu6500.h							Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#ifndef MPU6400_H
#define MPU6400_H

#include <stdio.h>
#include <math.h>
#include "twi_master.h"
#include "twi_master_config.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "swarmModule.h"


#define GYR_ADDR_W  0xD0	// 8-bit WRITE address for the mpu6500
#define GYR_ADDR_R  0xD1	// 8-bit READ address for the mpu6500

#define CONFIG 		0x1A	// 8-bit address for Configuration Register
#define INT_ST 		0x3A	// 8-bit address for Interupt Status Register

#define XOUT_H_A	0x3B	// 8-bit address for upper byte of X Accel Sensor
#define XOUT_L_A	0x3C	// 8-bit address for lower byte of X Accel Sensor
#define YOUT_H_A	0x3D	// 8-bit address for upper byte of Y Accel Sensor
#define YOUT_L_A	0x3E	// 8-bit address for lower byte of Y Accel Sensor
#define ZOUT_H_A	0x3F	// 8-bit address for upper byte of Z Accel Sensor
#define ZOUT_L_A	0x40	// 8-bit address for lower byte of Z Accel Sensor

#define XOUT_H_G 	0x43	// 8-bit address for upper byte of X Gyro Sensor
#define XOUT_L_G 	0x44	// 8-bit address for lower byte of X Gyro Sensor
#define YOUT_H_G 	0x45	// 8-bit address for upper byte of Y Gyro Sensor
#define YOUT_L_G 	0x46	// 8-bit address for lower byte of Y Gyro Sensor
#define ZOUT_H_G	0x47	// 8-bit address for upper byte of Z Gyro Sensor
#define ZOUT_L_G 	0x48	// 8-bit address for lower byte of Z Gyro Sensor
#define GYRO_CONFIG	0x1B
 
#define FS_SEL_0_GYRO		131.0	// LSB per deg/s for +/-250 deg/s res
//#define FS_SEL_0_GYRO		32.8
#define FS_SEL_0_ACCEL		16384.0	// LSB per g for +/-2 g, where g = 9.81 m/s^2

#define G_CONSTANT			9810.0	// 9.81 m/s^2 = 9810.0 mm/s^2

#define PI 3.14159265359

int16_t OFFSET_GYRO, OFFSET_ACCEL;
void mpu6500Init(void);

int mpu6500Status(void);

float mpu6500Readz_GYRO(uint8_t returnRAW);

float mpu6500Readx_ACCEL(uint8_t returnRAW);



#endif /* MPU6400_H */

