/**********************************************************************************************
*
* mpu6500.c							Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/



#include "mpu6500.h"
// Between Stop and Start condition require 1.3 us
//Mention FIFO -> probably not practical due to RT nature of WiSaR
//XOUT_H = 0x43
//XOUT_L = 0x44
//YOUT_H = 0x45
//YOUT_L = 0x46
//ZOUT_H = 0x47
//ZOUT_L = 0x48
//GYRO_CONFIG = 0x1B
// Checking the Data Ready interrupt. 


void mpu6500Init(void)
{
	/*uint8_t data[2] = {CONFIG, 0x00};
	//Set Sampling Rate in Config Register
	//Disable FIFO Mode
	twi_master_transfer(GYR_ADDR_W, data, 2, 1);	
	nrf_delay_ms(5);*/
	//uint8_t data[2] = {GYRO_CONFIG, 0x08};
	//twi_master_transfer(GYR_ADDR_W, data, 2, 1);
	
	//--- grab gyro/accel offsets ---/
	OFFSET_GYRO = 0;
	OFFSET_ACCEL = 0;
	while ((OFFSET_GYRO == 0) || (OFFSET_ACCEL == 0)) {
		getOffsets(&OFFSET_GYRO, &OFFSET_ACCEL);
	}

	return;
}


int mpu6500Status(void)
{
	uint8_t data[2] = {INT_ST, 0};

	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	
	return (data[0] & 0x01);
}


float mpu6500Readz_GYRO(uint8_t returnRAW)
{
	while(!mpu6500Status());	// wait for new set of data
	
	uint8_t zl = 0, zh = 0;		//define the MSB and LSB
	
	uint8_t data[2] = {ZOUT_L_G, 0};
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);

	nrf_delay_us(3);		//needs at least 1.3 us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	zl = data[0];			// receive the byte
	
	nrf_delay_us(3);		//needs at least 1.3 us free time between start and stop
	
	data[0] = ZOUT_H_G;
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	zh = data[0];			// receive the byte
	
	int16_t zout = (zl | (zh << 8));	//concatenate the MSB and LSB

	if (returnRAW) {
		return (float)zout;
	}
	else {
		return (((float)zout - (float)OFFSET_GYRO) / FS_SEL_0_GYRO);
	}
}


float mpu6500Readx_ACCEL(uint8_t returnRAW)
{
	while(!mpu6500Status());	// wait for new set of data
	
	uint8_t xl = 0, xh = 0;		//define the MSB and LSB
	
	uint8_t data[2] = {XOUT_L_A, 0};
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);

	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	xl = data[0];			// receive the byte
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	data[0] = XOUT_H_A;
	// transmit to device 0x0E
	// x MSB reg
	twi_master_transfer(GYR_ADDR_W, data, 1, 0);
	
	nrf_delay_us(3);		//needs at least 1.3us free time between start and stop
	
	twi_master_transfer(GYR_ADDR_R, data, 1, 1);
	xh = data[0];			// receive the byte
	
	int16_t xout = (xl | (xh << 8));	//concatenate the MSB and LSB

	if (returnRAW) {
		return (float)xout;
	}
	else {
		// -1 required since MPU6500 is backwards
		return ((-1.0)*(((float)xout - (float)OFFSET_ACCEL) / FS_SEL_0_ACCEL));
	}
}


