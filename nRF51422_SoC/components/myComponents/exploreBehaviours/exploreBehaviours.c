/**********************************************************************************************
*
* exploreBehaviours.c				Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#include "exploreBehaviours.h"



// call function as adjustDistance(&v_x, &v_y, ...)

void repulseRobots(float *F_x, float *F_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData)
{
	// initialize all variables
	*F_x = 0;
	*F_y = 0;
	float magMovement = 0;
	
	for (unsigned short counter = 0; counter < NUM_OF_CHANNELS; counter++) {
		//--- ignore if greater than DIS_FAR ---/
		if (proximityData[counter] > DIS_FAR) {
			// do nothing...
		}
		else {
			// if the values are from our channel, ignore
			if (counter == myChannel) {
				magMovement = 0;
			}
			else {
				//magMovement = REPEL_ROBS * (1.0/pow(((float)proximityData[counter]), 2));
				//magMovement = K_N * (1.0/pow(((float)proximityData[counter]), 2));
				magMovement = K_N * (1.0/((float)proximityData[counter]));
			}
			// sum up our x and y vectors
			*F_x -= magMovement * cos((float)bearingData[counter]*(PI/180.0));
			*F_y -= magMovement * sin((float)bearingData[counter]*(PI/180.0));
		}
	}
}


void repulseObstacles(float *F_x, float *F_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData)
{
	// initialize all variables
	float magMovement = 0;
	
	//--- ignore if greater than DIS_FAR ---/
	if (proximityData[myChannel] <= DIS_OBS) {			
		//magMovement = REPEL_OBS * (1.0/pow(((float)proximityData[myChannel]), 2));
		magMovement = K_O * (1.0/pow(((float)proximityData[myChannel]), 2));
		// sum up our x and y vectors
		*F_x -= magMovement * cos((float)bearingData[myChannel]*(PI/180.0));
		*F_y -= magMovement * sin((float)bearingData[myChannel]*(PI/180.0));
	}
}

