/**********************************************************************************************
*
* exploreBehaviours.h				Copyright (C) 2016 Justin Y. Kim and U of T MIE
*														last edit: April 5, 2016
*
*/


#ifndef EXPLORE_BEHAVIOURS_H
#define EXPLORE_BEHAVIOURS_H


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "swarmModule.h"
#include "motorSchema.h"




uint8_t K_O_TEMP, K_N_TEMP, V_STOP_TEMP;
float K_O, K_N, V_STOP;




#define DIS_FAR			80.0	// 90 mm
#define REPEL_ROBS		1.1

void repulseRobots(float *F_x, float *F_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData);



#define	DIS_OBS			20		// 30 mm
#define REPEL_OBS		200.5

void repulseObstacles(float *F_x, float *F_y, uint8_t myChannel, uint8_t *proximityData, uint16_t *bearingData);



#endif /* EXPLORE_BEHAVIOURS_H */
