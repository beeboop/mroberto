/***********************************************************************************************
*
*	chainBehaviours.h
*
*						Copyright (c) Justin Y. Kim and University of Toronto MIE 2016
*
*
*/

#ifndef CHAIN_BEHAVIOURS_H
#define CHAIN_BEHAVIOURS_H


#if (MY_CHANNEL == 1)
	#define CONST_PERP_DIS		15	// mm
#elif (MY_CHANNEL == 2)
	#define CONST_PERP_DIS		11	// mm
#elif (MY_CHANNEL == 3)
	#define CONST_PERP_DIS		8	// mm
#elif (MY_CHANNEL == 4)
	#define CONST_PERP_DIS		10	// mm
#elif (MY_CHANNEL == 5)
	#define CONST_PERP_DIS		4	// mm
#elif (MY_CHANNEL == 6)
	#define CONST_PERP_DIS		5	// mm
#elif (MY_CHANNEL == 8)
	#define CONST_PERP_DIS		0	// mm
#else
	#define CONST_PERP_DIS		8	// mm
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "swarmModule.h"
#include "motorSchema.h"



volatile uint8_t currentState;



void goToNest(float *v_x, float *v_y, uint8_t distanceLim, uint8_t nestID, uint8_t *proximityData, uint16_t *bearingData);


#define REP_SCALE		0.01
#define FOR_SCALE		2.0

#if (MY_CHANNEL == 1)
	#define ALLOWED_DIFF	13		// mm
#elif (MY_CHANNEL == 2)
	#define ALLOWED_DIFF	11		// mm
#elif (MY_CHANNEL == 3)
	#define ALLOWED_DIFF	8		// mm
#elif (MY_CHANNEL == 4)
	#define ALLOWED_DIFF	8		// mm
#elif (MY_CHANNEL == 5)
	#define ALLOWED_DIFF	4		// mm
#elif (MY_CHANNEL == 6)
	#define ALLOWED_DIFF	4		// mm
#elif (MY_CHANNEL == 8)
	#define ALLOWED_DIFF	1		// mm
#else
	#define ALLOWED_DIFF	8		// mm
#endif


void orbitRobotClkWise(float *v_x, float *v_y, uint8_t distanceLim, uint8_t robotNum2Orbit, uint8_t *proximityData, uint16_t *bearingData);



#endif /* CHAIN_BEHAVIOURS_H */


