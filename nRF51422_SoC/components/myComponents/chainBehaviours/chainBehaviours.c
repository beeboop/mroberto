

#include "chainBehaviours.h"



void goToNest(float *v_x, float *v_y, uint8_t distanceLim, uint8_t nestID, uint8_t *proximityData, uint16_t *bearingData)
{
	// initialize all variables
	*v_x = 0;
	*v_y = 0;
	
	if (proximityData[nestID] > (D_FAR + 10)) {
		return;
	}
	
	float magMovement = ATT_SCALE * (((float)proximityData[nestID]) - distanceLim);
	// sum up our x and y vectors
	*v_x = magMovement * cos((float)bearingData[nestID]*(PI/180.0));
	*v_y = magMovement * sin((float)bearingData[nestID]*(PI/180.0));
	
	return;
}



// call function as adjustDistance(&v_x, &v_y, ...)

void orbitRobotClkWise(float *v_x, float *v_y, uint8_t distanceLim, uint8_t robotNum2Orbit, uint8_t *proximityData, uint16_t *bearingData)
{
	// initialize all variables
	*v_x = 0;
	*v_y = 0;
	
	if (proximityData[robotNum2Orbit] > D_FAR) {
		return;
	}
	
	float repMoveMag = 0.0;
	
	//--- calculate repulsive or attractive force ---/
	if (abs((int16_t)distanceLim - (int16_t)proximityData[robotNum2Orbit]) > ALLOWED_DIFF) {
		repMoveMag = REP_SCALE * (((float)distanceLim) - ((float)proximityData[robotNum2Orbit]));
		*v_x += repMoveMag * cos(((float)bearingData[robotNum2Orbit] - 180.0) * (PI/180.0));
		*v_y += repMoveMag * sin(((float)bearingData[robotNum2Orbit] - 180.0) * (PI/180.0));
	}
	
	//--- calculate forward force that's perpendicular to the target robot ---/
	*v_x += FOR_SCALE * cos((((float)bearingData[robotNum2Orbit]) + 90.0) * (PI/180.0));
	*v_y += FOR_SCALE * sin((((float)bearingData[robotNum2Orbit]) + 90.0) * (PI/180.0));
	
	return;
}




